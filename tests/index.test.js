const expect = require('chai').expect;

describe('index', () => {
    it('converts a string passed as an argument to sentence case', () => {
       
        process.argv[2] = 'tHis is AN inPUt string';
        const oldLog = console.log
        
        let consoleOut;
        console.log = (...args) => {
            consoleOut = args
        };

        const cut = require('../index.js');
        console.log = oldLog

        expect(consoleOut[0]).to.equal('This is an input string');
    })
});