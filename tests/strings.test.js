
const strings = require('../strings.js')
const expect = require('chai').expect

describe('strings', () => {
    describe('makeSentenceCase', () => {
        it('returns a string', () => {
           let input = 'some Input';
           let result = strings.makeSentenceCase(input);
           expect(result).to.be.a('string') 
        })
        
        it('returns null if input is null', () => {
            let input = null;
            let result = strings.makeSentenceCase(input)
            expect(result).to.be.a('null')
        })

        it('returns undefined if input is undefined', () => {
            let result = strings.makeSentenceCase(undefined)
            expect(result).to.be.undefined
        })

        it('returns error if input is Date Object', () => {
            let input = new Date()
            let result = strings.makeSentenceCase(input)
            expect(result).to.be.an('error')
        })

        it('returns error if input is integer', () => {
            let input = 42
            let result = strings.makeSentenceCase(input)
            expect(result).to.be.an('error')
        })

        it('returns error if input is an array', () => {
            let input = [1, 'f', true]
            let result = strings.makeSentenceCase(input)
            expect(result).to.be.an('error')
        })

        it('returns error if input is an object', () => {
            let objectInput = {}
            let objectResult = strings.makeSentenceCase(objectInput)

            expect(objectResult).to.be.an('error')
        })

        it('capitalises first letter of input string', () => {
            let oneWordInput = 'single'
            let oneWordResult = strings.makeSentenceCase(oneWordInput)
            expect(oneWordResult).to.equal('Single')

            let twoWordInput = 'double words'
            let twoWordResult = strings.makeSentenceCase(twoWordInput)
            expect(twoWordResult).to.equal('Double words')
        })
        
        it('makes all other characters lower case', () => {
            let allCapsInput = 'ALLCAPS'
            let allCapsResult = strings.makeSentenceCase(allCapsInput)
            expect(allCapsResult).to.equal('Allcaps')

            let allCapsInput2 = 'ALLCAPS INPUT'
            let allCapsResult2 = strings.makeSentenceCase(allCapsInput2)
            expect(allCapsResult2).to.equal('Allcaps input')
        })

        it('handles mixed cases', () => {
            let mixedInput = 'miXEd CaSe'
            let mixedResult = strings.makeSentenceCase(mixedInput)

            expect(mixedResult).to.equal('Mixed case')
        })

    })
})
