
function makeSentenceCase(input){
    if(input === null){
        return null;
    }
    if(!input){
        return undefined
    }
    if(typeof input !== 'string'){
        return new Error('Invalid input')
    }
    let outputArray = input.split('').map((letter, index) => {
        if(index === 0){
            return letter.toUpperCase()
        }
        return letter.toLowerCase()
    })
    return outputArray.join('')
}



module.exports = { makeSentenceCase }