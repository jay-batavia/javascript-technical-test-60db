const strings = require('./strings')

//null case
console.log("Input: ", null, "\nOuput: ", strings.makeSentenceCase(null), '\n')

//undefined case
console.log("Input: ", undefined, "\nOuput: ", strings.makeSentenceCase(undefined), '\n')

//Date Object
let dateExample = new Date()
console.log("Input: ",  typeof dateExample, "\nOuput: ", strings.makeSentenceCase(dateExample).toString(), '\n')

//boolean
console.log("Input: ", typeof true, "\nOuput: ", strings.makeSentenceCase(true).toString(), '\n')

//integer
console.log("Input: ", 42, "\nOuput: ", strings.makeSentenceCase(42).toString(), '\n')

//array
console.log("Input: ", [1, 'b', false], "\nOuput: ", strings.makeSentenceCase([1, 'b', false]).toString(), '\n')

//object
console.log("Input: ", {}, "\nOuput: ", strings.makeSentenceCase({}).toString(), '\n')

//function
console.log("Input: ", 'function', "\nOuput: ", strings.makeSentenceCase(() => {console.log('hello')}).toString(), '\n')