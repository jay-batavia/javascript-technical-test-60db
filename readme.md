#README

This is a small, simple node project that implements a function `makeSentenceCase`. This function, if given a string, returns that string in sentence case. It capitalises the first word in the string and makes all other characters lower case.

## Run the function
 - Clone this repository
 - Navigate into the cloned directory
 - Run:
```
$ node index.js "this is an input String"
This is an input strinig
```

The `index.js` file is the "entry point" for this little app. The `index.js` script reads the string to convert from the command line arguments and then passes it to the `makeSentenceCase` function as an input parameter. It then logs out the resulting sentence case string to the console.

## Run the unit tests
```
//make sure to install the testing dependencies
$ npm install
...
//run the tests
$ npm test
```

The `tests/` directory contains two test files. `strings.test.js` contains unit tests for the `makeSentenceCase` function. It tests cases for different input types and values. This projet uses [`mocha`](https://mochajs.org/) as the testing framework and [`chai`](https://www.chaijs.com/) as its assertion library.

## Notes
 - The implementation of the solution uses `string.prototype.split` to tokenize the string and then uses `array.prototype.map` to apply the sentence case logic to each element.
 - The main implementation goes charcter by character, converts the first character to upperr case, and converts all the rest to lower case. 
 - There is an alternate implementation in the `more-functional` branch. *That* implementation tokenizes the string each word instead of character and has a helper function that capitalises the first word.
 - The `index.test.js` script can be thought of as an acceptance test, since it essentially mimics the steps a user would take to use this app. 
 - The `examples.js` script can be run on its own - `node examples.js` - to display various input cases (including different types) and their respective outputs.
